---
- name: Update OS packages
  apt:
    name: "{{ item }}"
    update_cache: yes
    state: present
  with_items:
    - aptitude
    - python-mysqldb

- name: Generate locales
  command: locale-gen {{ item }}
  with_items: "{{ t3bs.system_locales }}"

- name: Set default locale
  replace:
    path: /etc/default/locale
    regexp: "{{ item.regexp }}"
    replace: "{{ item.replace }}"
  with_items:
    - regexp: "^LANG=.*"
      replace: "LANG={{ t3bs.default_locale }}"
    - regexp: "^LANGUAGE=.*"
      replace: "LANGUAGE={{ t3bs.default_locale.split('.')[0] }}:"

- name: Remove old packages
  apt:
     autoremove: yes

- name: Add PHP 7.2 PPA
  apt_repository:
    repo: 'ppa:ondrej/php'

- name: Install required packages
  apt: name={{ item }} state=present
  with_items: "{{ t3bs.requirements }}"

- name: Download nodejs installer
  get_url:
    url: https://deb.nodesource.com/setup_6.x
    dest: /tmp/nodejs-installer.sh
  when: t3bs.install_nodejs == "yes"

- name: Execute nodejs installer
  script: /tmp/nodejs-installer.sh
  when: t3bs.install_nodejs == "yes"

- name: Install nodejs
  apt:
    name: nodejs
    update_cache: yes
    state: present
  when: t3bs.install_nodejs == "yes"

- name: Set default PHP version
  alternatives:
    name: php
    path: "{{ t3bs.php_bin_path }}/php{{ t3bs.php_version }}"

- name: Update php settings (apache2)
  ini_file:
    path: /etc/php/{{ t3bs.php_version }}/apache2/php.ini
    section: "{{ item.section }}"
    option: "{{ item.option }}"
    value: "{{ item.value }}"
    backup: yes
  with_items: "{{ t3bs.php_ini }}"

- name: Update php settings (cli)
  ini_file:
    path: /etc/php/{{ t3bs.php_version }}/cli/php.ini
    section: "{{ item.section }}"
    option: "{{ item.option }}"
    value: "{{ item.value }}"
    backup: yes
  with_items: "{{ t3bs.php_ini_cli }}"

- name: Download composer
  get_url:
    url: https://getcomposer.org/installer
    dest: /tmp/composer-setup.php
    mode: 0755
    force: yes

- name: Install composer
  shell: php /tmp/composer-setup.php --install-dir={{ t3bs.composer_bin_path }} --filename={{ t3bs.composer_bin }}
  args:
    creates: "{{ t3bs.composer_bin_path }}/{{ t3bs.composer_bin }}"

- name: Update composer
  shell: "{{ t3bs.composer_bin_path }}/{{ t3bs.composer_bin }} --quiet self-update"

- name: Create/Update www user
  user:
    name: "{{ t3bs.www_user }}"
    groups: "{{ t3bs.www_group }}"
    append: yes
    home: "{{ t3bs.www_user_home }}"
    shell: /bin/bash
    generate_ssh_key: yes
    state: present

- name: Set random password for www user
  shell: echo {{ t3bs.www_user }}:`pwgen -yB 16 1` | chpasswd
  when: t3bs.www_user != "vagrant"

- name: Set apache to run under different user/group
  lineinfile:
    dest: /etc/apache2/envvars
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
    state: present
  with_items:
    - { regexp: "^export APACHE_RUN_USER", line: "export APACHE_RUN_USER={{ t3bs.www_user }}"}
    - { regexp: "^export APACHE_RUN_GROUP", line: "export APACHE_RUN_GROUP={{ t3bs.www_group }}"}
  notify: restart apache

- name: Create Webroot
  file:
    path: "{{ t3bs.document_root }}"
    state: directory
    mode: 0755

- name: Create webroot symlink
  become: yes
  become_user: "{{ t3bs.www_user }}"
  file:
    src: "{{ t3bs.document_root }}"
    dest: "{{ t3bs.www_user_home }}/www"
    state: link

- name: Update apache vhost configuration
  template:
    src: ../templates/apache.conf.j2
    dest: /etc/apache2/sites-available/000-default.conf
    mode: 0644
  notify: restart apache

- name: Enable apache modules
  command: a2enmod {{ item }}
  with_items: "{{ t3bs.apache_modules }}"
  notify: restart apache

- name: Set mysql root password
  mysql_user: name=root host={{ item }} password={{ t3bs.db_root_password }} state=present
  with_items:
    - localhost
    - 127.0.0.1
    - ::1
  notify: flush privileges

- name: Secure mysql installation
  command: 'mysql -ne "{{ item }}"'
  with_items:
    - DELETE FROM mysql.user WHERE User=''
    - DROP DATABASE IF EXISTS test
    - DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'
  notify: flush privileges

- name: Drop mysql database
  mysql_db:
    name: "{{ t3bs.db_name }}"
    state: absent
  when: t3bs.reset_db_on_provision

- name: Create mysql database
  mysql_db:
    name: "{{ t3bs.db_name }}"
    state: present

- name: Create mysql user
  mysql_user:
    name: "{{ t3bs.db_user }}"
    password: "{{ t3bs.db_pass }}"
    priv: '{{ t3bs.db_name}}.*:ALL'
    state: present
  notify: flush privileges

- name: Link phpMyAdmin configuration
  file:
    src: /etc/phpmyadmin/apache.conf
    dest: /etc/apache2/conf-available/phpmyadmin.conf
    state: link

- pear:
    name: pecl/apcu
    state: present

- name: Enable phpMyAdmin
  command: a2enconf phpmyadmin
  notify: restart apache

#- name: init typo3
#  shell: ../../site/init_typo3_links.sh